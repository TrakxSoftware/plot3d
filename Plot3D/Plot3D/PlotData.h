//
//  PlotData.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Plot3DLib.h"


@interface PlotData : NSObject

+ (PlotData*)plotDataWithCSVURL:(NSURL*)url;
+ (PlotData*)plotData;

- (const Plot3DLib::Vector3fVec&)getPoints DEPRECATED_ATTRIBUTE;

- (const Plot3DLib::Vector3fVec&)getAPoints;
- (const Plot3DLib::Vector3fVec&)getGPoints;
- (const Plot3DLib::Vector3fVec&)getMPoints;

- (const Plot3DLib::Vector3fVec&)getAccelXPoints;
- (const Plot3DLib::Vector3fVec&)getAccelYPoints;
- (const Plot3DLib::Vector3fVec&)getAccelZPoints;

@end
