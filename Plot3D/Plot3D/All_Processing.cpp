#include "All_Processing.h"
#include <iostream>
//#include <boost/tuple/tuple.hpp>
//#include "gnuplot-iostream/gnuplot-iostream.h"


using namespace std;


void AllProcessing::Integrate(std::vector<float>* dataarray, float offset)
{
    
    std::vector<float>Tempdataarray(dataarray->size());
    for( int m=1; m<int(dataarray->size()); m++ )
    {
        Tempdataarray[m] = (*dataarray)[m];
        
    }
    for( int i=1; i<int(dataarray->size()); i++ )
    {
        (*dataarray)[i] = ((Tempdataarray[i-1]+ Tempdataarray[i])/2 - offset )*(SAMPLING_PERIOD)/2000 + (*dataarray)[i-1] ;
    }
    
}

void AllProcessing::ConvertToRadians(std::vector<float>* dataarray)
{
    for( int m=1; m<int(dataarray->size()); m++ )
    {
        (*dataarray)[m] = (*dataarray)[m]* M_PI/180;
        
    }
    
}

void AllProcessing::LowPassFilter(std::vector<float>* dataarray)
{
    
    for( int i=0; i<int(dataarray->size()); i++ )
    {
        MovingAverage(dataarray, i, MOV_AVG_COUNT);
    }
    return;
}

// Average of array elements starting from start_index and averaged over next Length number of elements
void AllProcessing::MovingAverage(std::vector<float>* dataarray, int start_index, int Length)
{
    if(dataarray->size() > start_index + Length - 1 )
    {
        for(int i=1;i<Length;i++)
            (*dataarray)[start_index] = (*dataarray)[start_index] + (*dataarray)[start_index + i];
        (*dataarray)[start_index] = (*dataarray)[start_index] / Length;
    }
    else
    {
        for(int k=1;k<=dataarray->size() - start_index - 1;k++)
            (*dataarray)[start_index] = (*dataarray)[start_index] + (*dataarray)[start_index + k];
        (*dataarray)[start_index] = (*dataarray)[start_index] / (dataarray->size() - start_index );
    }
    return;
}


void AllProcessing::loadFromCSV( const std::string& filename )
{
    std::ifstream       file( filename.c_str() );
    std::string                line;
    std::string                cell;
    int i;
    char **ptr;
    std::size_t* pos;
    float cell_value;
    
    while( file )
    {
//        std::getline(file,line);
        std::getline(file,line, '\r');
        std::stringstream lineStream(line);
        i=AX_POS;
        
        while( std::getline( lineStream, cell, ',' ) )
        {
            
            //float cell_value = std::stof(cell, pos);
            //float cell_value = boost::lexical_cast<float>(cell);
            std::stringstream convertor(cell);
            convertor >> cell_value;
            if(i==AX_POS)
                Ax.push_back(cell_value); //std::stof(cell));
            else if(i==AY_POS)
                Ay.push_back(cell_value);
            else if(i==AZ_POS)
                Az.push_back(cell_value);
            else if(i==GX_POS)
                Gx.push_back(cell_value);
            else if(i==GY_POS)
                Gy.push_back(cell_value);
            else if(i==GZ_POS)
                Gz.push_back(cell_value);
            else if(i==MX_POS)
                Mx.push_back(cell_value);
            else if(i==MY_POS)
                My.push_back(cell_value);
            else if(i==MZ_POS)
                Mz.push_back(cell_value);
            else
            {
                cout << "Extra Columns in input file: Please check the" << endl;
                break;
            }
            i++;
            
        }
        
    }
    
}
void AllProcessing::main(const std::string &file_name)
{
        int num_samples;
        //FOR PLotting in GNU plot
        //Gnuplot gp;

//	cout << "Enter input file name \n"; cin >> file_name;

        loadFromCSV (file_name);

	for( int i=0; i<int(Ax.size()); i++ )
          {
             std::cout << Ax[i] ; std::cout << std::endl;
          }

	LowPassFilter(&Ax);

        for( int i=0; i<int(Ax.size()); i++ )
          {
            std::cout << Ax[i] << " "<< Ay[i] << " "<< Az[i] << " "<< Gx[i] << " "<< Gy[i] << " "<< Gz[i] << " "<< Mx[i] << " "<< My[i] << " "<< Mz[i] << " ";

            std::cout << std::endl;
          }

        //Integrate(&Ax);
        //Integrate(&Ay);
        //Integrate(&Az);

        for( int i=0; i<int(Ax.size()); i++ )
          {
             std::cout << Ax[i] ; std::cout << std::endl;
          }
       //After Loading from the input cvs file, process the data one by one and store it in Estmiated acc points
        for( int i=0; i<int(Ax.size()); i++ )
          {
             //Rearrange the Accel data in vector form
//             acc_point.x = Ax[i];
//             acc_point.y = Ay[i];
//             acc_point.z = Az[i];
              acc_point[0] = Ax[i];
              acc_point[1] = Ay[i];
              acc_point[2] = Az[i];

             //Normalize the Vector
              acc_point.normalize();
//             normalizeVector(&acc_point);

             Acc.push_back(acc_point);

             //Rearrange the Gyro data in vector form
//             gyro_point.x = Gx[i];
//             gyro_point.y = Gy[i];
//             gyro_point.z = Gz[i];
              gyro_point[0] = Gx[i];
              gyro_point[1] = Gy[i];
              gyro_point[2] = Gz[i];
             //Normalize the Vector
              gyro_point.normalize();
//             normalizeVector(&gyro_point);
             Gyro.push_back(gyro_point);

             //Rearrange the Mag data in vector form
//             mag_point.x = Mx[i];
//             mag_point.y = My[i];
//             mag_point.z = Mz[i];
              mag_point[0] = Mx[i];
              mag_point[1] = My[i];
              mag_point[2] = Mz[i];
              mag_point.normalize();
//              normalizeVector(&mag_point);
             Mag.push_back(mag_point); // Check if the Mag values also need to be normalized
          }

//	estimate_accel_vector(&Acc, &Gyro, &Angle, &Est_Acc);
}


void  AllProcessing::estimate_accel_vector(std::vector<Vector> *Acc, std::vector<Vector> * Gyro, std::vector<Vector> * Angle, std::vector<Vector> * Est_Acc)
{
    for( int i=0; i<Acc->size(); i++ )
    {
        if(i==0)
        {
            
            Est_Acc->push_back((*Acc)[i]);
        }
        else
        {
            // Uncomment later if(abs((*Est_Acc)[i-1].z) < MIN_THR_ACC)
            // Uncomment later     {
            // Uncomment later       //If the previous estimated values is too small and because it is used as reference for computing
            // Uncomment later       //new angles, it's error fluctuations will amplify leading to bad results
            // Uncomment later       //in this case skip the gyro data and just use previous estimate
            // Uncomment later       (*Gyro)[i].x = (*Est_Acc)[i].x;
            // Uncomment later       (*Gyro)[i].y = (*Est_Acc)[i].y;
            // Uncomment later       (*Gyro)[i].z = (*Est_Acc)[i].z;
            // Uncomment later     }
            // Uncomment laterelse {
            //get angles between projection of R on ZX/ZY plane and Z axis, based on last Est_Acc[i-1]
            
            //Determine the Angle w. r. t. Z axis based on Est_Acc[i-1]
            //Angle.x is the Angle Axz which means angle between the project of vector on xz plane and z axis
            //Angle.y is the Angle Ayz which means angle between the project of vector on yz plane and z axis  (TBD: To think how to mke this logic more generic )
            
//            (*Angle)[i-1].x = atan2((*Est_Acc)[i-1].x,(*Est_Acc)[i-1].z) * 180 /M_PI;
            (*Angle)[i-1][0] = atan2((*Est_Acc)[i-1][0],(*Est_Acc)[i-1][2]) * 180 /M_PI;
//            (*Angle)[i].x = (*Angle)[i-1].x  + (((*Gyro)[i-1].x + (*Gyro)[i].x)/2)*SAMPLING_PERIOD/1000;
            (*Angle)[i][0] = (*Angle)[i-1][0]  + (((*Gyro)[i-1][0] + (*Gyro)[i][0])/2)*SAMPLING_PERIOD/1000;
            
//            (*Angle)[i-1].y = atan2((*Est_Acc)[i-1].y,(*Est_Acc)[i-1].z) * 180 /M_PI;
            (*Angle)[i-1][1] = atan2((*Est_Acc)[i-1][1],(*Est_Acc)[i-1][2]) * 180 /M_PI;
//            (*Angle)[i].y = (*Angle)[i-1].y  + (((*Gyro)[i-1].y + (*Gyro)[i].y)/2)*SAMPLING_PERIOD/1000;
            (*Angle)[i][1] = (*Angle)[i-1][1]  + (((*Gyro)[i-1][1] + (*Gyro)[i][1])/2)*SAMPLING_PERIOD/1000;
            
            
            
            
            //estimate sign of vector by looking in what qudrant the angle Angle[i].x is,
            //Gyro.x is pozitive if  Angle[i].x in range -90 ..90 => cos(Awz) >= 0
//            signofzcomponent = ( cos((*Angle)[i].x *M_PI / 180) >=0 ) ? 1 : -1;
            signofzcomponent = ( cos((*Angle)[i][0] *M_PI / 180) >=0 ) ? 1 : -1;
            
            //reverse calculation of Gyro from Angle[i] angles
//            (*Gyro)[i].x = sin((*Angle)[i].x *M_PI / 180);
            (*Gyro)[i][0] = sin((*Angle)[i][0] *M_PI / 180);
//            (*Gyro)[i].x /= sqrt( 1 + pow(cos((*Angle)[i].x *M_PI / 180), 2.0) * pow(tan((*Angle)[i].y *M_PI / 180), 2.0) );
            (*Gyro)[i][0] /= sqrt( 1 + pow(cos((*Angle)[i][0] *M_PI / 180), 2.0) * pow(tan((*Angle)[i][1] *M_PI / 180), 2.0) );
            
//            (*Gyro)[i].y = sin((*Angle)[i].y *M_PI / 180);
            (*Gyro)[i][1] = sin((*Angle)[i][1] *M_PI / 180);
//            (*Gyro)[i].y /= sqrt( 1 + pow(cos((*Angle)[i].y *M_PI / 180), 2.0) * pow(tan((*Angle)[i].x *M_PI / 180), 2.0) );
            (*Gyro)[i][1] /= sqrt( 1 + pow(cos((*Angle)[i][1] *M_PI / 180), 2.0) * pow(tan((*Angle)[i][0] *M_PI / 180), 2.0) );
            
//            (*Gyro)[i].z = signofzcomponent * sqrt(1 -  pow((*Gyro)[i].x, 2.0) - pow((*Gyro)[i].y, 2.0));
            (*Gyro)[i][2] = signofzcomponent * sqrt(1 -  pow((*Gyro)[i][0], 2.0) - pow((*Gyro)[i][1], 2.0));
            
            
            // Uncomment later}
            
        }
        
//        est_point.x = (*Acc)[i].x + WFACTOR*(*Gyro)[i].x / (1 + WFACTOR);
        est_point[0] = (*Acc)[i][0] + WFACTOR*(*Gyro)[i][0] / (1 + WFACTOR);
//        est_point.y = (*Acc)[i].y + WFACTOR*(*Gyro)[i].y / (1 + WFACTOR);
        est_point[1] = (*Acc)[i][1] + WFACTOR*(*Gyro)[i][1] / (1 + WFACTOR);
//        est_point.z = (*Acc)[i].z + WFACTOR*(*Gyro)[i].z / (1 + WFACTOR);
        est_point[2] = (*Acc)[i][2] + WFACTOR*(*Gyro)[i][2] / (1 + WFACTOR);
        
        normalizeVector(&est_point);
        Est_Acc->push_back(est_point);
        
    }
    
    
}
void  AllProcessing::normalizeVector(Vector * vector_of_points)
{
    //convert to a vector with same direction and with length 1
    float R, x, y, z;
//    x = vector_of_points->x;
    x = (*vector_of_points)[0];
//    y = vector_of_points->y;
    y = (*vector_of_points)[1];
//    z = vector_of_points->z;
    z = (*vector_of_points)[2];
    R = sqrt(x*x+y*y+z*z);
//    vector_of_points->x /= R;
    (*vector_of_points)[0] /= R;
//    vector_of_points->y /= R;
    (*vector_of_points)[1] /= R;
//    vector_of_points->z /= R;
    (*vector_of_points)[2] /= R;
    
}
//Unused functions. TO be ingored for now....

bool AllProcessing::zerocrossing(float A[], int i1, int i2)
{
    if(i1 > i2)
    {
        return zerocrossing(A, i2, i1 );
    }
    else if(A[i1]==0)
    {
        return zerocrossing(A, i1-1, i2);
    }
    else if(A[i2]==0)
    {
        return zerocrossing(A, i1, i2+1);
    }
    else
    {
        return ((A[i1]/A[i2] < 0) || (A[i2]/A[i1] < 0));
    }
}

void AllProcessing::average(float A[], int len, int nh)
{
    int B[len];
    for(int count1 = 0; count1 < len; count1 ++)
    {
        B[count1] = A[count1];
    }
    if(nh > 1)
    {
        float c;
        
        for(int count = 0; count < (nh/2); count ++)
        {
            c = 0;
            for(int cnt = 0; cnt <= 2*count; cnt ++)
            {
                c = c + B[cnt];
            }
            if(count == 0){
                A[count] = c;
            }
            else{
                A[count] = c/(2*count);
            }
        }
        for(int count = nh/2; count < (len - nh/2); count ++)
        {
            
            c = 0;
            for(int cnt = count - nh/2; cnt <= (count + nh/2); cnt ++)
            {
                c = c + B[cnt];
            }
            A[count] = c/nh;
        }
    }
    
}
