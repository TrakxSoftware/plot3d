//
//  State.m
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import "State.h"

static State *instance = nil;


@implementation State

+ (State*)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[State alloc] init];
    });
    
    return instance;
}

@end
