//
//  RenderView.m
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import "RenderView.h"
#import "Plot3DLib.h"
#import "PlotData.h"
#import "State.h"
#import <OpenGL/gl.h>
#import <CoreVideo/CoreVideo.h>

NSString *kPlotDataChangedNotificaionName = @"PlotDataChangeNotificaionName";
NSString *kPlotTypeChangedNotificationName = @"PlotTypeChangedNotificationName";

using Plot3DPtrVec = std::vector<Plot3DLib::Plot3DPtr>;

@interface RenderView ()
{
    Plot3DLib::Plot3DPtr mPlot3D DEPRECATED_ATTRIBUTE;
    Plot3DPtrVec mPlots;
    Plot3DLib::Scene3DPtr mScene;
    
    CVDisplayLinkRef mDisplayLink;
    CGFloat mLastFrameTime;
}

- (void)updateAnimation:(CGFloat)time;

@end


static CVReturn displayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* displayLinkContext)
{
    RenderView *renderView = (__bridge RenderView*)displayLinkContext;
    CGFloat time = CGFloat(now->videoTime) / CGFloat(now->videoTimeScale);
    [renderView updateAnimation:time];
    
    return kCVReturnSuccess;
}


@implementation RenderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPlotDataChangedNotification:) name:kPlotDataChangedNotificaionName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPlotTypeChangedNotification:) name:kPlotTypeChangedNotificationName object:nil];
    
    [self setupAnimationUpdate];
}

- (void)dealloc
{
    [self destroyAnimationUpdate];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupAnimationUpdate
{
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    CVDisplayLinkCreateWithActiveCGDisplays(&mDisplayLink);
    CVDisplayLinkSetOutputCallback(mDisplayLink, &displayLinkCallback, (__bridge void*)self);
    
    CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(mDisplayLink, cglContext, cglPixelFormat);
    
    CVDisplayLinkStart(mDisplayLink);
}

- (void)destroyAnimationUpdate
{
    CVDisplayLinkStop(mDisplayLink);
    CVDisplayLinkRelease(mDisplayLink);
}

- (void)prepareOpenGL
{
    using namespace Plot3DLib;
    mScene.reset(new Scene3D);
    mScene->setRootNode(NodePtr(new Node));
    
    [self reconfigureScene];
}

- (void)drawRect:(NSRect)dirtyRect
{
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    mScene->renderAll();
    
    glFlush();
}

- (void)updateAnimation:(CGFloat)time
{
    if (mLastFrameTime > 0.f)
    {
        CGFloat dTime = time - mLastFrameTime;
        if (mScene)
        {
            mScene->updateAll(dTime);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setNeedsDisplay:YES];
            });
        }
    }
    
    mLastFrameTime = time;
}

- (void)updateRenderPointBuffer
{
    [self.openGLContext makeCurrentContext];
    
    using namespace Plot3DLib;
    
    switch ([State sharedInstance].plotType)
    {
        case PT_Acceleration:
        {
            const Vector3fVec &data = [[PlotData plotData] getAPoints];
            VBOPtr buffer(new VBO(GL_ARRAY_BUFFER, 3));
            buffer->attachData(data.data(), u32(data.size() * sizeof(Vector3f)), GL_STATIC_DRAW);
            Plot3DPtr plot = mPlots[0];
            plot->setPointsBuffer(buffer);
            plot->setColor(Vector3f(1.f, 0.f, 0.f));
            
            break;
        }
            
        case PT_Gyroscope:
        {
            const Vector3fVec &data = [[PlotData plotData] getGPoints];
            VBOPtr buffer(new VBO(GL_ARRAY_BUFFER, 3));
            buffer->attachData(data.data(), u32(data.size() * sizeof(Vector3f)), GL_STATIC_DRAW);
            Plot3DPtr plot = mPlots[0];
            plot->setPointsBuffer(buffer);
            plot->setColor(Vector3f(0.f, 1.f, 0.f));
            
            break;
        }
            
        case PT_Magnetometer:
        {
            const Vector3fVec &data = [[PlotData plotData] getMPoints];
            VBOPtr buffer(new VBO(GL_ARRAY_BUFFER, 3));
            buffer->attachData(data.data(), u32(data.size() * sizeof(Vector3f)), GL_STATIC_DRAW);
            Plot3DPtr plot = mPlots[0];
            plot->setPointsBuffer(buffer);
            plot->setColor(Vector3f(0.f, 0.f, 1.f));
            
            break;
        }
            
        case PT_AccelerationXYZ:
        {
            // Update Accel X plot data
            {
                const Vector3fVec &data = [[PlotData plotData] getAccelXPoints];
                VBOPtr buffer(new VBO(GL_ARRAY_BUFFER, 3));
                buffer->attachData(data.data(), u32(data.size() * sizeof(Vector3f)), GL_STATIC_DRAW);
                Plot3DPtr plot = mPlots[0];
                plot->setPointsBuffer(buffer);
                plot->setColor(Vector3f(1.f, 0.f, 1.f));
            }
            
            // Update Accel Y plot data
            {
                const Vector3fVec &data = [[PlotData plotData] getAccelYPoints];
                VBOPtr buffer(new VBO(GL_ARRAY_BUFFER, 3));
                buffer->attachData(data.data(), u32(data.size() * sizeof(Vector3f)), GL_STATIC_DRAW);
                Plot3DPtr plot = mPlots[1];
                plot->setPointsBuffer(buffer);
                plot->setColor(Vector3f(0.f, 1.f, 1.f));
            }
            
            // Update Accel Z plot data
            {
                const Vector3fVec &data = [[PlotData plotData] getAccelZPoints];
                VBOPtr buffer(new VBO(GL_ARRAY_BUFFER, 3));
                buffer->attachData(data.data(), u32(data.size() * sizeof(Vector3f)), GL_STATIC_DRAW);
                Plot3DPtr plot = mPlots[2];
                plot->setPointsBuffer(buffer);
                plot->setColor(Vector3f(1.f, 1.f, 0.f));
            }
            
            break;
        }
    }
}

- (void)reconfigureScene
{
    using namespace Plot3DLib;
    
    switch ([State sharedInstance].plotType)
    {
        case PT_Acceleration:
        case PT_Gyroscope:
        case PT_Magnetometer:
        {
            if (mPlots.size() > 1 || mPlots.size() == 0)
            {
                NodePtr rootNode(new Node);
                rootNode->addSceneObject(Axes3DPtr(new Axes3D));
                
                mPlots.clear();
                
                Plot3DPtr plot(new Plot3D);
                plot->setPointsBuffer(VBOPtr(new VBO(GL_ARRAY_BUFFER, 3)));
                rootNode->addSceneObject(plot);
                mPlots.push_back(plot);
                
                mScene->setRootNode(rootNode);
            }
            break;
        }
            
        case PT_AccelerationXYZ:
        {
            if (mPlots.size() < 3 || mPlots.size() == 0)
            {
                NodePtr rootNode(new Node);
                rootNode->addSceneObject(Axes3DPtr(new Axes3D));
                
                mPlots.clear();
                
                for (int i = 0; i < 3; ++i)
                {
                    Plot3DPtr plot(new Plot3D);
                    plot->setPointsBuffer(VBOPtr(new VBO(GL_ARRAY_BUFFER, 3)));
                    rootNode->addSceneObject(plot);
                    mPlots.push_back(plot);
                }
                
                mScene->setRootNode(rootNode);
            }
            
            break;
        }
    }
}

- (void)rotateScene:(CGFloat)angle
{
    using namespace Plot3DLib;
    
    if (mScene)
    {
        mScene->rotate(angle);
    }
}

- (void)scale:(CGFloat)magnification
{
    using namespace Plot3DLib;
    
    if (mScene)
    {
        mScene->scale(1.f + magnification);
    }
}

- (void)scrollWheel:(NSEvent *)theEvent
{
    using namespace Plot3DLib;
    
    if (mScene)
    {
        const CGFloat sign = theEvent.deltaY / fabs(theEvent.deltaY);
        mScene->scale(1.f + sign * 0.1);
    }
}

#pragma mark - Notifications

- (void)onPlotDataChangedNotification:(NSNotification*)notification
{
    [self updateRenderPointBuffer];
    [self setNeedsDisplay:YES];
}

- (void)onPlotTypeChangedNotification:(NSNotification*)notification
{
    [self reconfigureScene];
    [self updateRenderPointBuffer];
    [self setNeedsDisplay:YES];
}

@end
