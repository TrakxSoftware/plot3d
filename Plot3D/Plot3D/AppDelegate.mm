//
//  AppDelegate.m
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import "AppDelegate.h"
#import "PlotData.h"
#import "State.h"

extern NSString *kPlotDataChangedNotificaionName;
extern NSString *kPlotTypeChangedNotificationName;

@interface AppDelegate ()

@end


@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    // Insert code here to tear down your application
}

- (BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
    if (menuItem.action == @selector(openDocument:))
        return YES;
    
    if ([PlotData plotData] != nil)
    {
        if (menuItem.action == @selector(onAcceleration:))
        {
            menuItem.state = [State sharedInstance].plotType == PT_Acceleration ? NSOnState : NSOffState;
        }
        
        if (menuItem.action == @selector(onGyroscope:))
        {
            menuItem.state = [State sharedInstance].plotType == PT_Gyroscope ? NSOnState : NSOffState;
        }
        
        if (menuItem.action == @selector(onMagnetometer:))
        {
            menuItem.state = [State sharedInstance].plotType == PT_Magnetometer ? NSOnState : NSOffState;
        }
        
        if (menuItem.action == @selector(onAccelerationXYZ:))
        {
            menuItem.state = [State sharedInstance].plotType == PT_AccelerationXYZ ? NSOnState : NSOffState;
        }
        
        return YES;
    }
    
    return NO;
}

#pragma mark - Actions

- (IBAction)openDocument:(NSMenuItem*)sender
{
    NSOpenPanel *openDialog = [NSOpenPanel openPanel];
    openDialog.canChooseFiles = YES;
    openDialog.canChooseDirectories = NO;
    [openDialog setAllowedFileTypes:@[@"csv"]];
    
    if ([openDialog runModal] == NSModalResponseOK)
    {
        NSArray * urls = openDialog.URLs;
        if (urls.count > 0)
        {
            [PlotData plotDataWithCSVURL:urls[0]];
            [State sharedInstance].plotType = PT_Acceleration;
            [[NSNotificationCenter defaultCenter] postNotificationName:kPlotDataChangedNotificaionName object:nil];
        }
    }
}

- (IBAction)onAcceleration:(NSMenuItem*)sender
{
    [State sharedInstance].plotType = PT_Acceleration;
    [[NSNotificationCenter defaultCenter] postNotificationName:kPlotTypeChangedNotificationName object:nil];
}

- (IBAction)onGyroscope:(NSMenuItem*)sender
{
    [State sharedInstance].plotType = PT_Gyroscope;
    [[NSNotificationCenter defaultCenter] postNotificationName:kPlotTypeChangedNotificationName object:nil];
}

- (IBAction)onMagnetometer:(NSMenuItem*)sender
{
    [State sharedInstance].plotType = PT_Magnetometer;
    [[NSNotificationCenter defaultCenter] postNotificationName:kPlotTypeChangedNotificationName object:nil];
}

- (IBAction)onAccelerationXYZ:(NSMenuItem*)sender
{
    [State sharedInstance].plotType = PT_AccelerationXYZ;
    [[NSNotificationCenter defaultCenter] postNotificationName:kPlotTypeChangedNotificationName object:nil];
}

@end
