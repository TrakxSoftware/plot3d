//
//  ViewController.m
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import "ViewController.h"
#import "RenderView.h"
#import "State.h"

extern NSString *kPlotTypeChangedNotificationName;


@interface ViewController ()
@property (nonatomic, weak) IBOutlet RenderView *renderView;
@property (nonatomic, weak) IBOutlet NSView *legendView;
@property (nonatomic, weak) IBOutlet NSView *axColorView;
@property (nonatomic, weak) IBOutlet NSView *ayColorView;
@property (nonatomic, weak) IBOutlet NSView *azColorView;

@property (nonatomic, assign) CGPoint previousLocation;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPlotTypeChangedNotification:) name:kPlotTypeChangedNotificationName object:nil];
    
    [self updateUI];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateUI
{
    self.axColorView.layer.backgroundColor = [NSColor colorWithCalibratedRed:1.f green:0.f blue:1.f alpha:1.f].CGColor;
    self.ayColorView.layer.backgroundColor = [NSColor colorWithCalibratedRed:0.f green:1.f blue:1.f alpha:1.f].CGColor;
    self.azColorView.layer.backgroundColor = [NSColor colorWithCalibratedRed:1.f green:1.f blue:0.f alpha:1.f].CGColor;
    
    self.legendView.hidden = ([State sharedInstance].plotType != PT_AccelerationXYZ);
}

#pragma mark - Actions

- (IBAction)onMagnification:(NSMagnificationGestureRecognizer*)sender
{
    [self.renderView scale:sender.magnification];
}

- (IBAction)onPan:(NSPanGestureRecognizer*)sender
{
    switch (sender.state)
    {
        case NSGestureRecognizerStateBegan:
        {
            self.previousLocation = [sender locationInView:self.view];
            break;
        }
            
        case NSGestureRecognizerStateChanged:
        {
            CGPoint location = [sender locationInView:self.view];
            CGFloat sign = location.x - self.previousLocation.x > 0 ? 1.f : -1.f;
            self.previousLocation = location;
            
            [self.renderView rotateScene:0.1f * sign];
            break;
        }
            
        default:break;
    }
}

#pragma mark - Notifications

- (void)onPlotTypeChangedNotification:(NSNotification*)notification
{
    [self updateUI];
}

@end
