attribute vec4 position;

varying vec4 colorVarying;

uniform mat4 modelViewProjectionMatrix;
uniform vec3 color;
//uniform float pointSize;

void main()
{
    colorVarying = vec4(color.r, color.g, color.b, 1.0);
//    gl_PointSize = pointSize;
    gl_PointSize = 20.0;
    gl_Position = modelViewProjectionMatrix * position;
}
