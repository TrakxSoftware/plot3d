
#ifndef Plot3d_All_Processing_h
#define Plot3d_All_Processing_h

#include "Plot3DLib.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream> 
#include <math.h>



#define AX_POS 1
#define AY_POS 2
#define AZ_POS 3
#define GX_POS 4
#define GY_POS 5
#define GZ_POS 6
#define MX_POS 7
#define MY_POS 8
#define MZ_POS 9
#define MOV_AVG_COUNT 5
#define SAMPLING_PERIOD 50 //in miliseconds
#define MIN_THR_ACC 0.1
#define WFACTOR 3

class AllProcessing
{
public:
    
    using Vector = Plot3DLib::Vector3f;
    
//    struct Vector {
//        float x,y,z;
//        
//    }
    Vector acc_point, gyro_point, mag_point, est_point;
    
    bool zerocrossing(float A[], int i1, int i2);
    void average(float A[], int len, int nh);
    void loadFromCSV( const std::string& filename );
    void LowPassFilter(std::vector<float>* dataarray);
    void MovingAverage(std::vector<float>* dataarray, int start_index, int Length);
    void Integrate(std::vector<float>* dataarray, float offset = 0);
    void ConvertToRadians(std::vector<float>* dataarray);
    void  normalizeVector(Vector * vector_of_points);
    void  estimate_accel_vector(std::vector<Vector> *Acc, std::vector<Vector> * Gyro,  std::vector<Vector> * Angle, std::vector<Vector> * Est_Acc);
    void main(const std::string &file_name);
    
    std::vector< std::vector<std::string> >   motion_data; //Motion Data is in the format Ax,Ay,Az,Gx,Gy,Gz,Mx,My,Mz
    std::vector<float>  Ax,Ay,Az,Gx,Gy,Gz,Mx,My,Mz; //Motion Data is in the format Ax,Ay,Az,Gx,Gy,Gz,Mx,My,Mz
    std::vector<Vector> Acc, Est_Acc, Angle, Gyro, Mag;
    bool first_sample = false;
    int signofzcomponent;

};

#endif