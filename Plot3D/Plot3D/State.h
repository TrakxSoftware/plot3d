//
//  State.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, PlotType)
{
    PT_Acceleration = 0,
    PT_Gyroscope,
    PT_Magnetometer,
    PT_AccelerationXYZ,
};

@interface State : NSObject
@property (nonatomic, assign) PlotType plotType;

+ (State*)sharedInstance;

@end
