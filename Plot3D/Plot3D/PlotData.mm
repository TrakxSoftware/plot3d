//
//  PlotData.m
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import "PlotData.h"
#import <fstream>
#import "All_Processing.h"

typedef NS_ENUM(NSUInteger, ComponentPosition)
{
    CP_X = 0,
    CP_Y,
    CP_Z,
};

typedef NS_ENUM(NSUInteger, PointPosition)
{
    PP_AX_POS = 1,
    PP_AY_POS = 2,
    PP_AZ_POS = 3,
    PP_GX_POS = 4,
    PP_GY_POS = 5,
    PP_GZ_POS = 6,
    PP_MX_POS = 7,
    PP_MY_POS = 8,
    PP_MZ_POS = 9,
};

static PlotData *instance = nil;


@interface PlotData ()
{
    Plot3DLib::Vector3fVec mPoints DEPRECATED_ATTRIBUTE;
    
    Plot3DLib::Vector3fVec mAPoints DEPRECATED_ATTRIBUTE;
    Plot3DLib::Vector3fVec mGPoints DEPRECATED_ATTRIBUTE;
    Plot3DLib::Vector3fVec mMPoints DEPRECATED_ATTRIBUTE;
    
    Plot3DLib::Vector3fVec mAccelXPoints;
    Plot3DLib::Vector3fVec mAccelYPoints;
    Plot3DLib::Vector3fVec mAccelZPoints;
    
    AllProcessing mAllProcessing;
}

@end


@implementation PlotData

+ (PlotData*)plotDataWithCSVURL:(NSURL*)url
{
    PlotData *plotData = [[PlotData alloc] init];
    if ([plotData loadFromCSV:url])
    {
        instance = plotData;
        return [PlotData plotData];
    }

    return nil;
}

+ (PlotData*)plotData
{
    return instance;
}

- (BOOL)loadFromCSV:(NSURL*)url
{
    mAllProcessing.main([url.path UTF8String]);
    [self prepareAccelXYZPoints];
    return YES;
}

- (BOOL) loadPlainData:(NSURL*)url DEPRECATED_ATTRIBUTE
{
    mPoints.clear();
    
    NSString *fileData = [NSString stringWithContentsOfURL:url
                                                  encoding:NSUTF8StringEncoding
                                                     error:nil];
    if (fileData)
    {
        using namespace Plot3DLib;
        
        ComponentPosition position = CP_X;
        Vector3f point;
        
        NSArray *rows = [fileData componentsSeparatedByString:@"\n"];
        for (NSString *row in rows)
        {
            NSArray *cols = [row componentsSeparatedByString:@","];
            for (NSString *col in cols)
            {
                switch (position)
                {
                    case CP_X:
                    {
                        point = Vector3f();
                        point[position] = [col floatValue];
                        position = CP_Y; // Next value will be Y
                        break;
                    }
                        
                    case CP_Y:
                    {
                        point[position] = [col floatValue];
                        position = CP_Z; // Next value will be Z
                        break;
                    }
                        
                    case CP_Z:
                    {
                        point[position] = [col floatValue];
                        mPoints.push_back(point);
                        position = CP_X; // Next value will be X
                        break;
                    }
                }
            }
        }
        
        return YES;
    }
    
    return NO;
}

- (BOOL)loadFromCSVFile:(NSURL*)url DEPRECATED_ATTRIBUTE
{
    if (!url)
        return NO;
    
    using namespace Plot3DLib;
    String path(url.fileSystemRepresentation);
    std::ifstream file(path);
    std::vector<float>  Ax,Ay,Az,Gx,Gy,Gz,Mx,My,Mz;
    
    if (!file.is_open())
        return NO;
    
    while(file)
    {
        std::string line;
        std::getline(file, line, '\r');
        std::stringstream lineStream(line);
        int i = PP_AX_POS;
        std::string cell;
        
        while( std::getline( lineStream, cell, ',' ) )
        {
            //float cell_value = std::stof(cell, pos);
            //float cell_value = boost::lexical_cast<float>(cell);
            std::stringstream convertor(cell);
            float cell_value = 0.f;
            convertor >> cell_value;
            switch (PointPosition(i))
            {
                case PP_AX_POS:
                    Ax.push_back(cell_value);
                    break;
                    
                case PP_AY_POS:
                    Ay.push_back(cell_value);
                    break;
                    
                case PP_AZ_POS:
                    Az.push_back(cell_value);
                    break;
                    
                case PP_GX_POS:
                    Gx.push_back(cell_value);
                    break;
                    
                case PP_GY_POS:
                    Gy.push_back(cell_value);
                    break;
                    
                case PP_GZ_POS:
                    Gz.push_back(cell_value);
                    break;
                    
                case PP_MX_POS:
                    Mx.push_back(cell_value);
                    break;
                    
                case PP_MY_POS:
                    My.push_back(cell_value);
                    break;
                    
                case PP_MZ_POS:
                    Mz.push_back(cell_value);
                    break;
                    
                default:
                {
                    std::cout << "Extra Column " << i << " in input file: Please check the" << std::endl;
                    break;
                }
            }
            
            ++i;
        }
    }
    
    // TODO : create vector3f data vectors

    {
        mAPoints.clear();
        const size_t pointsCount = MIN(MIN(Ax.size(), Ay.size()), Az.size());
        for (size_t i = 0; i < pointsCount; ++i)
        {
            mAPoints.push_back(Plot3DLib::Vector3f(Ax[i], Ay[i], Az[i]));
        }
    }

    {
        mGPoints.clear();
        const size_t pointsCount = MIN(MIN(Gx.size(), Gy.size()), Gz.size());
        for (size_t i = 0; i < pointsCount; ++i)
        {
            mGPoints.push_back(Plot3DLib::Vector3f(Gx[i], Gy[i], Gz[i]));
        }
    }

    {
        mMPoints.clear();
        const size_t pointsCount = MIN(MIN(Mx.size(), My.size()), Mz.size());
        for (size_t i = 0; i < pointsCount; ++i)
        {
            mMPoints.push_back(Plot3DLib::Vector3f(Mx[i], My[i], Mz[i]));
        }
    }
    
    return YES;
}

- (void)prepareAccelXYZPoints
{
    using namespace Plot3DLib;
    static const f32 kTimeStep = 0.01f;
    
    // Prepare Accel X data
    {
        mAccelXPoints.clear();
        f32 time = 0.f;
        for (Vector3f value : mAllProcessing.Acc)
        {
            mAccelXPoints.push_back(Vector3f(time, value[0], 0.f));
            time += kTimeStep;
        }
    }
    
    // Prepare Accel Y data
    {
        mAccelYPoints.clear();
        f32 time = 0.f;
        for (Vector3f value : mAllProcessing.Acc)
        {
            mAccelYPoints.push_back(Vector3f(time, value[1], 0.f));
            time += kTimeStep;
        }
    }
    
    // Prepare Accel Z data
    {
        mAccelZPoints.clear();
        f32 time = 0.f;
        for (Vector3f value : mAllProcessing.Acc)
        {
            mAccelZPoints.push_back(Vector3f(time, value[2], 0.f));
            time += kTimeStep;
        }
    }
}

- (const Plot3DLib::Vector3fVec&)getPoints
{
    return mPoints;
}

- (const Plot3DLib::Vector3fVec&)getAPoints
{
    return mAllProcessing.Acc;
}

- (const Plot3DLib::Vector3fVec&)getGPoints
{
    return mAllProcessing.Gyro;
}

- (const Plot3DLib::Vector3fVec&)getMPoints
{
    return mAllProcessing.Mag;
}

- (const Plot3DLib::Vector3fVec&)getAccelXPoints
{
    return mAccelXPoints;
}

- (const Plot3DLib::Vector3fVec&)getAccelYPoints
{
    return mAccelYPoints;
}

- (const Plot3DLib::Vector3fVec&)getAccelZPoints
{
    return mAccelZPoints;
}

@end
