//
//  RenderView.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RenderView : NSOpenGLView

- (void)scale:(CGFloat)magnification;
- (void)rotateScene:(CGFloat)angle;

@end
