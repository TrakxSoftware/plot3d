//
//  Utility.h
//  Trakx
//
//  Created by Aleksandar Dimov on 10/1/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject

+ (Utility *)sharedInstance;

- (CGSize)getSizeOfLabel:(UILabel *)label limitedToMaxSize:(CGSize)maxSize;

@end
