//
//  UserInfoCell.h
//  Trakx
//
//  Created by Aleksandar Dimov on 10/4/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andFrame:(CGRect)frame;

- (void)updateCellWithTitle:(NSString *)title;

@end
