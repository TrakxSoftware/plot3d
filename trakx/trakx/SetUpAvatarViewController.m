//
//  SetUpAvatarViewController.m
//  Trakx
//
//  Created by Aleksandar Dimov on 9/30/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import "SetUpAvatarViewController.h"
#import "SetUpCalibrationViewController.h"
#import "Constants.h"
#import "Utility.h"
#import "UserInfoCell.h"

@interface SetUpAvatarViewController ()
{
    NSArray *userInfoCellsTitlesArray;
}
@end

@implementation SetUpAvatarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Trakx";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(onNextButton)];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    userInfoCellsTitlesArray = @[@"HEIGHT", @"WEIGHT"];
    
    [self setupViews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews
{
    // title label
    CGFloat navigationBarOriginY = self.navigationController.navigationBar.frame.origin.y;
    CGFloat navigationBarHeight = self.navigationController.navigationBar.bounds.size.height;
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [titleLabel setFont:[UIFont systemFontOfSize:22.0]];
    [titleLabel setText:@"SELECT YOUR AVATAR"];
    CGSize titleLabelSize = [[Utility sharedInstance] getSizeOfLabel:titleLabel limitedToMaxSize:CGSizeMake(kScreenWidth, kScreenHeight)];
    [titleLabel setFrame:CGRectMake((kScreenWidth - titleLabelSize.width) / 2,
                                    navigationBarOriginY + navigationBarHeight + kScreenHeight * 0.04,
                                    titleLabelSize.width,
                                    titleLabelSize.height)];
    [self.view addSubview:titleLabel];
    
    // user info table view
    CGFloat tableViewHeight = kScreenHeight * 0.2;
    UITableView *userInfoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kScreenHeight - tableViewHeight, kScreenWidth, tableViewHeight)];
    [userInfoTableView setBackgroundColor:[UIColor blueColor]];
    [userInfoTableView setDataSource:self];
    [userInfoTableView setDelegate:self];
    [self.view addSubview:userInfoTableView];
}

- (void)onNextButton
{
    SetUpCalibrationViewController *setUpCalibrationViewController = [[SetUpCalibrationViewController alloc] init];
    [self.navigationController pushViewController:setUpCalibrationViewController animated:YES];
}

#pragma mark - UITableViewDataSource and UITableViewDelegateMethods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.frame.size.height / 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell Identifier";
    
    UserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[UserInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier andFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height / 2)];
    }
    
    NSString *cellTitle = [userInfoCellsTitlesArray objectAtIndex:indexPath.row];
    [cell updateCellWithTitle:cellTitle];
    
    return cell;
}

@end
