//
//  main.m
//  Trakx
//
//  Created by Aleksandar Dimov on 9/30/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
