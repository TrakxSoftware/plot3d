//
//  SetUpCalibrationViewController.m
//  Trakx
//
//  Created by Aleksandar Dimov on 9/30/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import "SetUpCalibrationViewController.h"
#import "SetUpLoginViewController.h"
#import "Utility.h"
#import "Constants.h"

@interface SetUpCalibrationViewController ()

@end

@implementation SetUpCalibrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Trakx";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(onBackButton)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(onNextButton)];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self setupViews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews
{
    CGFloat navigationBarOriginY = self.navigationController.navigationBar.frame.origin.y;
    CGFloat navigationBarHeight = self.navigationController.navigationBar.bounds.size.height;
    
    UILabel *topLabel = [[UILabel alloc] init];
    [topLabel setFont:[UIFont systemFontOfSize:18.0]];
    [topLabel setText:@"CALLIBRATE GLOVES"];
    CGSize topLabelSize = [[Utility sharedInstance] getSizeOfLabel:topLabel limitedToMaxSize:CGSizeMake(kScreenWidth, kScreenHeight)];
    [topLabel setFrame:CGRectMake((kScreenWidth - topLabelSize.width) / 2,
                                    navigationBarOriginY + navigationBarHeight + kScreenHeight * 0.04,
                                    topLabelSize.width,
                                    topLabelSize.height)];
    [self.view addSubview:topLabel];
    
    CGFloat arrowImageViewSize = kScreenHeight * 0.07;
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - arrowImageViewSize) / 2, topLabel.frame.origin.y + topLabel.frame.size.height + kScreenHeight * 0.05, arrowImageViewSize, arrowImageViewSize)];
    [arrowImageView setImage:[UIImage imageNamed:@"upArrow"]];
    [arrowImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:arrowImageView];
    
    CGFloat gloveImageViewSize = kScreenWidth * 0.6;
    UIImageView *gloveImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - gloveImageViewSize) / 2, arrowImageView.frame.origin.y + arrowImageView.frame.size.height + kScreenHeight * 0.1, gloveImageViewSize, gloveImageViewSize)];
    [gloveImageView setImage:[UIImage imageNamed:@"glove"]];
    [gloveImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:gloveImageView];
    
    UILabel *bottomLabel = [[UILabel alloc] init];
    [bottomLabel setFont:[UIFont systemFontOfSize:18.0]];
    [bottomLabel setText:@"MOVE GLOVES UP AND DOWN"];
    CGSize bottomLabelSize = [[Utility sharedInstance] getSizeOfLabel:bottomLabel limitedToMaxSize:CGSizeMake(kScreenWidth, kScreenHeight)];
    [bottomLabel setFrame:CGRectMake((kScreenWidth - bottomLabelSize.width) / 2,
                                  kScreenHeight - bottomLabelSize.height - kScreenHeight * 0.04,
                                  bottomLabelSize.width,
                                  bottomLabelSize.height)];
    [self.view addSubview:bottomLabel];
}

- (void)onBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onNextButton
{
    SetUpLoginViewController *setUpLoginViewController = [[SetUpLoginViewController alloc] init];
    [self.navigationController pushViewController:setUpLoginViewController animated:YES];
}

@end
