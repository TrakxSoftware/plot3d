//
//  Utility.m
//  Trakx
//
//  Created by Aleksandar Dimov on 10/1/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+ (Utility *)sharedInstance
{
    static Utility *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[Utility alloc] init];
    });
    
    return _sharedInstance;
}

- (CGSize)getSizeOfLabel:(UILabel *)label limitedToMaxSize:(CGSize)maxSize
{
    CGRect labelRect = [label.text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:label.font} context:nil];
    
    return CGSizeMake(labelRect.size.width, labelRect.size.height);
}

@end
