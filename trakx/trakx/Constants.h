//
//  Constants.h
//  Trakx
//
//  Created by Aleksandar Dimov on 10/1/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

#endif /* Constants_h */
