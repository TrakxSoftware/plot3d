//
//  SetUpLoginViewController.m
//  Trakx
//
//  Created by Aleksandar Dimov on 9/30/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import "SetUpLoginViewController.h"
#import "Utility.h"
#import "Constants.h"

@interface SetUpLoginViewController ()
{
    UITextField *activeTextField;
    NSArray *textFieldsArray;
}
@end

@implementation SetUpLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Trakx";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(onBackButton)];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureOccured:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    [self setupViews];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    // Unregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews
{
    CGFloat controllerViewWidth = self.view.bounds.size.width;
    CGFloat controllerViewHeight = self.view.bounds.size.height;
    CGFloat navigationBarOriginY = self.navigationController.navigationBar.frame.origin.y;
    CGFloat navigationBarHeight = self.navigationController.navigationBar.bounds.size.height;
    
    CGFloat inset = 20.0;
    
    // title label
    UILabel *titleLabel = [[UILabel alloc] init];
    [titleLabel setFont:[UIFont systemFontOfSize:24.0]];
    [titleLabel setText:@"SET UP"];
    CGSize titleLabelSize = [[Utility sharedInstance] getSizeOfLabel:titleLabel limitedToMaxSize:CGSizeMake(controllerViewWidth, controllerViewHeight)];
    [titleLabel setFrame:CGRectMake((controllerViewWidth - titleLabelSize.width) / 2,
                                    navigationBarOriginY + navigationBarHeight + controllerViewHeight * 0.04,
                                   titleLabelSize.width,
                                    titleLabelSize.height)];
    [self.view addSubview:titleLabel];
    
    // name label
    UILabel *nameLabel = [[UILabel alloc] init];
    [nameLabel setFont:[UIFont systemFontOfSize:20.0]];
    [nameLabel setText:@"NAME"];
    CGSize nameLabelSize = [[Utility sharedInstance] getSizeOfLabel:nameLabel limitedToMaxSize:CGSizeMake(controllerViewWidth, controllerViewHeight)];
    [nameLabel setFrame:CGRectMake(inset,
                                   titleLabel.frame.origin.y + titleLabel.frame.size.height + controllerViewHeight * 0.07,
                                   nameLabelSize.width,
                                   nameLabelSize.height)];
    [self.view addSubview:nameLabel];
    
    // first name text field
    CGFloat distance = 20.0;
    UITextField *firstNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width + distance, nameLabel.frame.origin.y, (controllerViewWidth - nameLabel.frame.size.width - 2 * inset - 2 * distance) / 2, nameLabel.frame.size.height)];
    [firstNameTextField setBackgroundColor:[UIColor whiteColor]];
    [firstNameTextField.layer setBorderColor:[UIColor blackColor].CGColor];
    [firstNameTextField.layer setBorderWidth:1.0];
    [firstNameTextField setDelegate:self];
    [firstNameTextField setReturnKeyType:UIReturnKeyNext];
    [self.view addSubview:firstNameTextField];
    
    // last name text field
    UITextField *lastNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(firstNameTextField.frame.origin.x + firstNameTextField.frame.size.width + distance, nameLabel.frame.origin.y, (controllerViewWidth - nameLabel.frame.size.width - 2 * inset - 2 * distance) / 2, nameLabel.frame.size.height)];
    [lastNameTextField setBackgroundColor:[UIColor whiteColor]];
    [lastNameTextField.layer setBorderColor:[UIColor blackColor].CGColor];
    [lastNameTextField.layer setBorderWidth:1.0];
    [lastNameTextField setDelegate:self];
    [lastNameTextField setReturnKeyType:UIReturnKeyNext];
    [self.view addSubview:lastNameTextField];
    
    // username label
    UILabel *usernameLabel = [[UILabel alloc] init];
    [usernameLabel setFont:[UIFont systemFontOfSize:20.0]];
    [usernameLabel setText:@"USERNAME"];
    CGSize usernameLabelSize = [[Utility sharedInstance] getSizeOfLabel:usernameLabel limitedToMaxSize:CGSizeMake(controllerViewWidth, controllerViewHeight)];
    [usernameLabel setFrame:CGRectMake(inset,
                                   nameLabel.frame.origin.y + nameLabel.frame.size.height + controllerViewHeight * 0.04,
                                   usernameLabelSize.width,
                                   usernameLabelSize.height)];
    [self.view addSubview:usernameLabel];
    
    // username text field
    UITextField *usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(usernameLabel.frame.origin.x + usernameLabel.frame.size.width + distance, usernameLabel.frame.origin.y, (controllerViewWidth - usernameLabel.frame.size.width - 2 * inset - distance), usernameLabel.frame.size.height)];
    [usernameTextField setBackgroundColor:[UIColor whiteColor]];
    [usernameTextField.layer setBorderColor:[UIColor blackColor].CGColor];
    [usernameTextField.layer setBorderWidth:1.0];
    [usernameTextField setDelegate:self];
    [usernameTextField setReturnKeyType:UIReturnKeyNext];
    [self.view addSubview:usernameTextField];
    
    // password label
    UILabel *passwordLabel = [[UILabel alloc] init];
    [passwordLabel setFont:[UIFont systemFontOfSize:20.0]];
    [passwordLabel setText:@"PASSWORD"];
    CGSize passwordLabelSize = [[Utility sharedInstance] getSizeOfLabel:passwordLabel limitedToMaxSize:CGSizeMake(controllerViewWidth, controllerViewHeight)];
    [passwordLabel setFrame:CGRectMake(inset,
                                       usernameLabel.frame.origin.y + usernameLabel.frame.size.height + controllerViewHeight * 0.04,
                                       passwordLabelSize.width,
                                       passwordLabelSize.height)];
    [self.view addSubview:passwordLabel];
    
    // password text field
    UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(passwordLabel.frame.origin.x + passwordLabel.frame.size.width + distance, passwordLabel.frame.origin.y, (controllerViewWidth - passwordLabel.frame.size.width - 2 * inset - distance), passwordLabel.frame.size.height)];
    [passwordTextField setBackgroundColor:[UIColor whiteColor]];
    [passwordTextField.layer setBorderColor:[UIColor blackColor].CGColor];
    [passwordTextField.layer setBorderWidth:1.0];
    [passwordTextField setSecureTextEntry:YES];
    [passwordTextField setDelegate:self];
    [passwordTextField setReturnKeyType:UIReturnKeyGo];
    [self.view addSubview:passwordTextField];
    
    textFieldsArray = @[firstNameTextField, lastNameTextField, usernameTextField, passwordTextField];
    
    // login button
    CGFloat loginButtonWidth = controllerViewWidth * 0.3;
    CGFloat loginButtonHeight = loginButtonWidth * 0.4;
    UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake((controllerViewWidth - loginButtonWidth) / 2, passwordLabel.frame.origin.y + passwordLabel.frame.size.height + controllerViewHeight * 0.1, loginButtonWidth, loginButtonHeight)];
    [loginButton addTarget:self action:@selector(onLoginButton) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"LOGIN" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [loginButton setBackgroundColor:[UIColor whiteColor]];
    [loginButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [loginButton.layer setBorderWidth:1.0];
    [self.view addSubview:loginButton];
}

- (void)onBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onLoginButton
{
    NSLog(@"Login button pressed!");
}

- (void)tapGestureOccured:(UITapGestureRecognizer *)sender
{
    if(activeTextField)
    {
        [activeTextField resignFirstResponder];
        activeTextField = nil;
    }
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect activeTextFieldFrame = [self.view convertRect:activeTextField.frame toView:[[UIApplication sharedApplication].delegate window]];
    CGFloat keyboardOriginY = kScreenHeight - keyboardSize.height;
    
    CGFloat overlappedDistance = activeTextFieldFrame.origin.y + activeTextFieldFrame.size.height - keyboardOriginY;
    
    if(overlappedDistance > 0.0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - overlappedDistance, self.view.frame.size.width, self.view.frame.size.height)];
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger index = [textFieldsArray indexOfObject:textField];
    
    if(index + 1 < textFieldsArray.count)
    {
        UITextField *nextTextField = (UITextField *)[textFieldsArray objectAtIndex:index + 1];
        [nextTextField becomeFirstResponder];
        activeTextField = nextTextField;
    }
    else
    {
        [self onLoginButton];
    }
    
    return YES;
}

@end
