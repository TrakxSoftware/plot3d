//
//  SetUpAvatarViewController.h
//  Trakx
//
//  Created by Aleksandar Dimov on 9/30/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetUpAvatarViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
