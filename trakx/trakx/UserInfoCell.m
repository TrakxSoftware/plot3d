//
//  UserInfoCell.m
//  Trakx
//
//  Created by Aleksandar Dimov on 10/4/15.
//  Copyright © 2015 Trakx. All rights reserved.
//

#import "UserInfoCell.h"

@interface UserInfoCell()
{
    UILabel *titleLabel;
}
@end

@implementation UserInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andFrame:(CGRect)frame
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        [self setFrame:frame];
        [self setupViews];
    }
    
    return self;
}

- (void)setupViews
{
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 0.0, (self.frame.size.width) / 2 - 2 * 20.0, self.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentLeft];
    [titleLabel setFont:[UIFont systemFontOfSize:20.0]];
    [self addSubview:titleLabel];
    
    CGFloat valueTextFieldHeight = 30.0;
    UITextField *valueTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.frame.size.width / 2 + 20.0, (self.frame.size.height - valueTextFieldHeight) / 2, (self.frame.size.width) / 2 - 2 * 20.0, valueTextFieldHeight)];
    [valueTextField.layer setBorderColor:[UIColor blackColor].CGColor];
    [valueTextField.layer setBorderWidth:1.0];
    [self addSubview:valueTextField];
}

- (void)updateCellWithTitle:(NSString *)title
{
    [titleLabel setText:title];
}

@end
