/// \file cmlincludes.h
/// CML library includes.
/// \author Alexander Svyatokho

#ifndef ARDRM_CMLINCLUDES_H
#define ARDRM_CMLINCLUDES_H

// Suppress some warnings from cml.
#if _MSC_VER > 1000
#   pragma warning(push)
#   pragma warning(disable : 4244)
#   pragma warning(disable : 4305)
#endif

#include "cml_config.h"         
#include <cmath>
#include <cml/cml.h>

#if _MSC_VER > 1000
#   pragma warning(pop)
#endif

#endif // ARDRM_CMLINCLUDES_H
