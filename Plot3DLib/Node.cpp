//
//  Node.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Node.h"
#include "SceneObject.h"

namespace Plot3DLib
{
    Node::Node()
    {        
    }

    Node::~Node()
    {
    }
    
    void Node::addChildNode(NodePtr node)
    {
        mChildNodes.push_back(node);
    }
    
    void Node::addSceneObject(SceneObjectPtr object)
    {
        mSceneObjects.push_back(object);
    }
    
    void Node::render(const Matrix4f &modelViewProjection)
    {
        Matrix4f localModelViewProjection = modelViewProjection * mTransformation;
        
        for (SceneObjectPtr object : mSceneObjects)
        {
            object->render(localModelViewProjection);
        }
        
        for (NodePtr node : mChildNodes)
        {
            node->render(localModelViewProjection);
        }
    }
    
    void Node::update(f32 dTime)
    {
        for (SceneObjectPtr object : mSceneObjects)
        {
            object->update(dTime);
        }
        
        for (NodePtr node : mChildNodes)
        {
            node->update(dTime);
        }
    }
}
