//
//  Texture2D.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Texture2D.h"
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl3.h>

namespace Plot3DLib
{
    namespace
    {
        GLenum glTextureFromTextureBindSlot(const TextureBindSlot slot)
        {
            if (slot > TBS_Count)
            {
                return GL_TEXTURE0 + TBS_Count - TBS_None - 1;
            }
            
            return GL_TEXTURE0 + slot - TBS_None - 1;
        }
    }
    
    Texture2D::Texture2D(size_t width, size_t height)
    : mId(0)
    , mBindSlot(TBS_None)
    , mWidth(width)
    , mHeight(height)
    {
    }

    Texture2D::~Texture2D()
    {
        destroy();
    }

    bool Texture2D::isValid() const
    {
        return (mId != 0);
    }

    bool Texture2D::generate()
    {
        glGenTextures(1, (GLuint*)&mId);
    
        bind();

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLint)getWidth(), (GLint)getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    
        unbind();
  
        return isValid();
    }

    bool Texture2D::destroy()
    {
        if (isValid())
        {
            glDeleteTextures(1, (GLuint*)&mId);
            mId = 0;
        }
        
        return !isValid();
    }

    void Texture2D::bind(TextureBindSlot slot /*= TBS_0*/)
    {
        if (isValid())
        {
            glActiveTexture(glTextureFromTextureBindSlot(slot));
            glBindTexture(GL_TEXTURE_2D, (GLuint)mId);
            
            mBindSlot = slot;
        }
    }

    void Texture2D::unbind()
    {
        if (isValid())
        {
            glActiveTexture(glTextureFromTextureBindSlot(mBindSlot));
            glBindTexture(GL_TEXTURE_2D, 0);
            
            mBindSlot = TBS_None;
        }
    }
}