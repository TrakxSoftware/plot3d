//
//  Plot3D.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_Plot3D_h
#define Plot3D_Plot3D_h

#include "SceneObject.h"

namespace Plot3DLib
{
    class Plot3D : public SceneObject
    {
    public:
        //
        Plot3D();
        //
        virtual ~Plot3D();
        //
        void render(const Matrix4f &modelViewProjection) override;
        //
        void update(f32 dTime) override;
        //
        void setPointsBuffer(VBOPtr value);
        //
        void setColor(const Vector3f &value) { mColor = value; }
        
    protected:
    private:
        //
        void initialize();
        //
        bool isValid() const;
        //
        ShaderProgramPtr mShaderProgram;
        //
        VBOPtr mPointsBuffer;
        //
        Vector3f mColor;
        //
        s32 mRenderPointsCount = 0;
        //
        f32 mAnimationTime = 0.f;
    };
}

#endif
