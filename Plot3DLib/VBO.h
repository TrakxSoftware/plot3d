//
//  VBO.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3d_VBO_h
#define Plot3d_VBO_h

#include "Types.h"

namespace Plot3DLib
{
    const u32 kInvalidBufferId = 0;
    
    class VBO
    {
    public:
        VBO(u32 target, u16 dimension);
        virtual ~VBO();
        
        
        bool isValid() const;
        
        void bind() const;
        void unbind() const;
        
        bool attachData(const void *data, u32 dataSize, u32 usage);
        
//        void *lockDataToWrite();
//        void unlockData();
        
//        VBOPtr copy() const;
        
        u16 getDimension() const { return mDimension; }
        u32 getDataSize() const { return mDataSize; }
        
    private:
        bool create(u32 target, u16 dimension);
        bool destroy();
        
        u32 mId = kInvalidBufferId;
        u32 mTarget = 0;
        u16 mDimension = 0;
        u32 mDataSize = 0;
        u32 mUsage = 0;
    };
}

#endif
