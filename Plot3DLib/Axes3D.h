//
//  Axes3D.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_Axes3D_h
#define Plot3D_Axes3D_h

#include "SceneObject.h"

namespace Plot3DLib
{
    class Axes3D : public SceneObject
    {
    public:
        //
        Axes3D();
        //
        virtual ~Axes3D();
        //
        void render(const Matrix4f &modelViewProjection) override;
        //
        void update(f32 dTime) override;        
        
    protected:
    private:
        //
        Vector3fVec mPointsX;
        //
        Vector3fVec mPointsY;
        //
        Vector3fVec mPointsZ;
        //
        Vector3fVec mPointsYNegative;
        //
        ShaderProgramPtr mShaderProgram;
    };
}

#endif
