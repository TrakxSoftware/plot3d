//
//  Types.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_Types_h
#define Plot3D_Types_h

#include <vector>
#include <map>
#include <string>
#include <memory>
#include "cmlincludes.h"

namespace Plot3DLib
{
    using u16 = unsigned short int;
    using u32 = unsigned int;
    using s32 = int;
    using f32 = float;
    using c8 = char;
    using String = std::string;
    
    enum TextureBindSlot
    {
        TBS_None = 0,
        TBS_0,
        TBS_1,
        TBS_2,
        TBS_3,
        TBS_4,
        TBS_5,
        TBS_Count
    };
    
    using Matrix4f = cml::matrix44f_c;
    using Matrix3f = cml::matrix33f_c;
    using Vector2f = cml::vector2f;
    using Vector3f = cml::vector3f;
    using Vector4f = cml::vector4f;
    
    class ShaderProgram;
    class Plot3D;
    class Camera3D;
    class VBO;
    class Scene3D;
    class Node;
    class SceneObject;
    class Axes3D;
    
    using ShaderProgramPtr = std::shared_ptr<ShaderProgram>;
    using Plot3DPtr = std::shared_ptr<Plot3D>;
    using Camera3DPtr = std::shared_ptr<Camera3D>;
    using VBOPtr = std::shared_ptr<VBO>;
    using Scene3DPtr = std::shared_ptr<Scene3D>;
    using NodePtr = std::shared_ptr<Node>;
    using SceneObjectPtr = std::shared_ptr<SceneObject>;
    using Axes3DPtr = std::shared_ptr<Axes3D>;
    
    using Vector3fVec = std::vector<Vector3f>;
    using SIPair = std::pair<String, s32>;
    using SIPairVec = std::vector<SIPair>;
    using NodePtrVec = std::vector<NodePtr>;
    using SceneObjectPtrVec = std::vector<SceneObjectPtr>;
}

#endif
