//
//  ShaderProgram.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "ShaderProgram.h"
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl3.h>
#include <iostream>


namespace Plot3DLib
{
    namespace
    {
        using SShaderProgramPtrMap = std::map<String, ShaderProgramPtr>;
        static SShaderProgramPtrMap shaderProgramsCache;
        
        const s32 INVALID_LOCATION  = -1;
        
        GLuint createShader(GLenum shaderType, const String & shaderSource)
        {
            GLuint shader = glCreateShader(shaderType);
            if (shader)
            {
                const char * source = shaderSource.c_str();
                glShaderSource(shader, 1, &source, NULL);
                glCompileShader(shader);
                GLint compiled = 0;
                glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
                if (!compiled)
                {
                    GLint infoLen = 0;
                    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
                    if (infoLen)
                    {
                        c8 * buf = new char[infoLen];
                        if (buf)
                        {
                            glGetShaderInfoLog(shader, infoLen, NULL, buf);
                            std::cout << String(buf) << std::endl;
                            delete [] buf;
                        }
                        glDeleteShader(shader);
                        shader = 0;
                    }
                }
            }
            
            return shader;
        }
    }
    
    ShaderProgramPtr ShaderProgram::create(const String & name, const String & vertexShaderCode, const String & fragmentShaderCode, const SIPairVec & preBindAttrinbs /*= SIPairVec()*/)
    {
        ShaderProgramPtr program;
        
        SShaderProgramPtrMap::const_iterator citer = shaderProgramsCache.find(name);
        if (citer != shaderProgramsCache.end())
        {
            program = citer->second;
        }
        else
        {
            program.reset(new ShaderProgram(name, vertexShaderCode, fragmentShaderCode, preBindAttrinbs));
            shaderProgramsCache.insert(SShaderProgramPtrMap::value_type(name, program));
        }
        
        return program;
    }
    
    void ShaderProgram::clearCache()
    {
        shaderProgramsCache.clear();
    }
    
    ShaderProgram::ShaderProgram(const String & name, const String & vertexShaderCode, const String & fragmentShaderCode, const SIPairVec & preBindAttrinbs /*= SIPairVec()*/)
    : mName(name)
    {
        create(vertexShaderCode, fragmentShaderCode, preBindAttrinbs);
    }
    
    ShaderProgram::~ShaderProgram()
    {
        destroy();
    }
    
    bool ShaderProgram::create(const String & vertexShaderCode, const String & fragmentShaderCode, const SIPairVec & preBindAttrinbs /*= SIPairVec()*/)
    {
        if (isValid())
            return true;
        
        mVertShader = createShader(GL_VERTEX_SHADER, vertexShaderCode);
        if (!mVertShader)
        {
            std::cout << "ShaderManager::createShaderShaderProgram(): Canot create vertex Shader" << std::endl;
            return false;
        }
        
        mFragShader = createShader(GL_FRAGMENT_SHADER,fragmentShaderCode);
        if (!mFragShader)
        {
            std::cout << "ShaderManager::createShaderShaderProgram(): Canot create fragment Shader" << std::endl;
            return false;
        }
        
        mShaderProgram = glCreateProgram();
        if (mShaderProgram > 0)
        {
            glAttachShader(mShaderProgram, mVertShader);
            glAttachShader(mShaderProgram, mFragShader);
            
            for (const SIPair & bindPair : preBindAttrinbs)
            {
                bindAttribLocation(bindPair.first, bindPair.second);
            }
            
            glLinkProgram(mShaderProgram);
            
            GLint linkStatus = GL_FALSE;
            glGetProgramiv(mShaderProgram, GL_LINK_STATUS, &linkStatus);
            
            if (linkStatus != GL_TRUE)
            {
                GLint bufLength = 0;
                glGetProgramiv(mShaderProgram, GL_INFO_LOG_LENGTH, &bufLength);
                if (bufLength)
                {
                    char* buf = new char[bufLength];
                    if (buf)
                    {
                        glGetProgramInfoLog(mShaderProgram, bufLength, NULL, buf);
                        std::cout << "Could not link ShaderProgram: " << buf << std::endl;
                        delete [] buf;
                    }
                }
                glDeleteProgram(mShaderProgram);
                mShaderProgram = 0;
                return false;
            }
            
            bind();
            //GLUtils::checkGLError(getCore().getLogger(), "glUseShaderProgram");
            glValidateProgram(mShaderProgram);
            //GLUtils::checkGLError(getCore().getLogger(), "glValidateShaderProgram");
            unbind();
        }
        else
        {
            std::cout << "ShaderManager::createShaderShaderProgram(): Canot create Shader ShaderProgram" << std::endl;
            return false;
        }
        
        return true;
    }
    
    bool ShaderProgram::destroy()
    {
        if (isValid())
        {
            glDetachShader(mShaderProgram, mVertShader);
            glDetachShader(mShaderProgram, mFragShader);
            glDeleteShader(mVertShader);
            glDeleteShader(mFragShader);
            glDeleteProgram(mShaderProgram);
            
            mShaderProgram = kInvalidShaderProgramId;
            
            return true;
        }
        
        return false;
    }
    
    const String & ShaderProgram::getName() const
    {
        return mName;
    }
    
    bool ShaderProgram::setUniform(const String & name, s32 value)
    {
        s32 location = getUniformLocation(name);
        if (location != INVALID_LOCATION)
        {
            glUniform1iv(location, 1, &value);
            return true;
        }
        
        return false;
        
    }
    
    bool ShaderProgram::setUniform(const String & name, f32 value)
    {
        s32 location = getUniformLocation(name);
        if (location != INVALID_LOCATION)
        {
            glUniform1fv(location, 1, &value);
            return true;
        }
        
        return false;
    }

    bool ShaderProgram::setUniform(const String & name, const Matrix4f & value)
    {
        s32 location = getUniformLocation(name);
        if (location != INVALID_LOCATION)
        {
            glUniformMatrix4fv(location, 1, GL_FALSE, value.data());
            return true;
        }
        
        return false;
    }
    
    bool ShaderProgram::setUniform(const String & name, const Matrix3f & value)
    {
        s32 location = getUniformLocation(name);
        if (location != INVALID_LOCATION)
        {
            glUniformMatrix3fv(location, 1, GL_FALSE, value.data());
            return true;
        }
        
        return false;
    }
    
    bool ShaderProgram::setUniform(const String & name, const Vector2f & value)
    {
        s32 location = getUniformLocation(name);
        if (location != INVALID_LOCATION)
        {
            glUniform2f(location, value[0], value[1]);
            return true;
        }
        
        return false;
    }
    
    bool ShaderProgram::setUniform(const String & name, const Vector3f & value)
    {
        s32 location = getUniformLocation(name);
        if (location != INVALID_LOCATION)
        {
            glUniform3f(location, value[0], value[1], value[2]);
            return true;
        }
        
        return false;
    }
    
    bool ShaderProgram::setVertexAttrib(const String & name, u16 dimension, const f32 * data)
    {
        return setVertexAttrib(name, dimension, 0, data);
    }
    
    bool ShaderProgram::setVertexAttrib(const String & name, u16 dimension, s32 stride, const f32 * data)
    {
        s32 location = getAttribLocation(name);
        if (location != INVALID_LOCATION)
        {
            glVertexAttribPointer(location, dimension, GL_FLOAT, GL_FALSE, stride, (GLvoid*)data);
            return true;
        }
        
        return false;
    }
    
    bool ShaderProgram::setVertexAttrib(s32 location, u16 dimension, s32 stride, const f32 *data)
    {
        if (location != INVALID_LOCATION)
        {                
            glVertexAttribPointer(location, dimension, GL_FLOAT, GL_FALSE, stride, (GLvoid*)data);
            return true;
        }
        
        return false;
    }
    
    s32 ShaderProgram::getUniformLocation(const String & name)
    {
        LocationMap::const_iterator citer = mUniformLocations.find(name);
        if (citer != mUniformLocations.end())
        {
            return citer->second;
        }
        
        s32 location = glGetUniformLocation(mShaderProgram, name.c_str());
        if ( location != INVALID_LOCATION )
        {
            mUniformLocations.insert(LocationMap::value_type(name, location));
        }
        
        return location;
    }
    
    s32 ShaderProgram::getAttribLocation(const String & name)
    {
        LocationMap::const_iterator citer = mAttribLocations.find(name);
        if (citer != mAttribLocations.end())
        {
            return citer->second;
        }
        
        s32 location = glGetAttribLocation(mShaderProgram, name.c_str());
        if ( location != INVALID_LOCATION )
        {
            mAttribLocations.insert(LocationMap::value_type(name, location));
        }
        
        return location;
    }
    
    bool ShaderProgram::bindAttribLocation(const String & name, s32 location)
    {
        if (location != INVALID_LOCATION)
        {
            glBindAttribLocation(mShaderProgram, location, name.c_str());
            
            LocationMap::iterator iter = mAttribLocations.find(name);
            if (iter != mAttribLocations.end())
            {
                iter->second = location;
            }
            else
            {
                mAttribLocations.insert(LocationMap::value_type(name, location));
            }
        }
        
        return true;
    }
    
    void ShaderProgram::bind()
    {
        glUseProgram(mShaderProgram);
    }
    
    void ShaderProgram::unbind()
    {
        glUseProgram(0);
    }
    
    bool ShaderProgram::isValid() const
    {
        return (mShaderProgram != kInvalidShaderProgramId);
    }
}
