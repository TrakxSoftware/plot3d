//
//  Consts.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_Consts_h
#define Plot3D_Consts_h

#include "Types.h"

namespace Plot3DLib
{
    namespace Consts
    {
        extern const String kSimpleShaderName;
        
        namespace Shader
        {
            extern const String kModelViewProjectionMatrixUniformName;
            extern const String kPositionAttribName;
            extern const String kColorUniformName;
        }
    }
}

#endif
