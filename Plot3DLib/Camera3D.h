//
//  Camera3D.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3d_Camera3D_h
#define Plot3d_Camera3D_h

#include "Types.h"

namespace Plot3DLib
{
    class Camera3D
    {
    public:
        //
        Matrix4f getCameraTransformationMatrix() const;
        //
        void translate(const Vector3f &value) { mPosition += value; }
        //
        void setPosition(const Vector3f &value) { mPosition = value; }
        //
        const Vector3f &getPosition() const { return mPosition; }
        
    private:
        //
        Vector3f mPosition = Vector3f(1.f, 1.f, 5.f);
    };
}

#endif
