//
//  SceneObject.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_SceneObject_h
#define Plot3D_SceneObject_h

#include "Types.h"

namespace Plot3DLib
{
    class SceneObject
    {
    public:
        //
        SceneObject() {}
        //
        virtual ~SceneObject() {}
        //
        virtual void render(const Matrix4f &modelViewProjection) = 0;
        //
        virtual void update(f32 dTime) = 0;
        
    protected:
    private:
    };
}

#endif
