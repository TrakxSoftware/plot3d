//
//  Consts.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Consts.h"

namespace Plot3DLib
{
    namespace Consts
    {
        const String kSimpleShaderName = "simple";
     
        namespace Shader
        {
            const String kModelViewProjectionMatrixUniformName = "modelViewProjectionMatrix";
            const String kPositionAttribName = "position";
            const String kColorUniformName = "color";
        }
    }
}
