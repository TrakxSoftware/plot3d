//
//  Camera3D.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Camera3D.h"

namespace Plot3DLib
{
    Matrix4f Camera3D::getCameraTransformationMatrix() const
    {
        Matrix4f mat = Matrix4f().identity();
        cml::matrix_look_at_RH(mat, mPosition, Vector3f(0.f, 0.f, 0.f), Vector3f(0.f, 1.f, 0.f));
        return mat.inverse();
    }
}
