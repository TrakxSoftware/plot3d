//
//  Axes3D.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Axes3D.h"
#include "Consts.h"
#include "ShaderProgram.h"
#include "FileUtils.h"
#include <OpenGL/gl3.h>

namespace Plot3DLib
{
    namespace
    {
        static const f32 kAxesLength = 3.f;
        static const f32 kDashStep = 0.02f;
        static const f32 kLineWidth = 1.f;
    }
    
    Axes3D::Axes3D()
    {
        mPointsX.push_back(Vector3f(0.f, 0.f, 0.f));
        mPointsX.push_back(Vector3f(kAxesLength, 0.f, 0.f));
        mPointsY.push_back(Vector3f(0.f, 0.f, 0.f));
        mPointsY.push_back(Vector3f(0.f, kAxesLength, 0.f));
        mPointsZ.push_back(Vector3f(0.f, 0.f, 0.f));
        mPointsZ.push_back(Vector3f(0.f, 0.f, kAxesLength));
        
        f32 y = 0.f;
        while (y > -kAxesLength)
        {
            y -= kDashStep;
            mPointsYNegative.push_back(Vector3f(0.f, y, 0.f));
        }
        
        String vertextShaderSource = FileUtils::loadNamedVertexShaderSource(Consts::kSimpleShaderName);
        String fragmentShaderSource = FileUtils::loadNamedFragmentShaderSource(Consts::kSimpleShaderName);
        mShaderProgram = ShaderProgram::create(Consts::kSimpleShaderName, vertextShaderSource, fragmentShaderSource, SIPairVec());
    }

    Axes3D::~Axes3D()
    {
    }
    
    void Axes3D::render(const Matrix4f &modelViewProjection)
    {
        glLineWidth(kLineWidth);
        
        //RenderData localData(data);
        
        if (!mShaderProgram || !mShaderProgram->isValid())
            return;
            
        mShaderProgram->bind();
        mShaderProgram->setUniform(Consts::Shader::kModelViewProjectionMatrixUniformName, modelViewProjection);
        
        
        // make sure we don't interfere with VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        // Draw X axis.
        s32 positionLocation = mShaderProgram->getAttribLocation("position");
        glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)mPointsX.data());
        glEnableVertexAttribArray(positionLocation);
        mShaderProgram->setUniform(Consts::Shader::kColorUniformName, Vector3f(1.f, 0.f, 0.f));
        glDrawArrays(GL_LINES, 0, (GLsizei)mPointsX.size());
        glDisableVertexAttribArray(positionLocation);
        
        // Draw Y axis.
        glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)mPointsY.data());
        glEnableVertexAttribArray(positionLocation);
        mShaderProgram->setUniform(Consts::Shader::kColorUniformName, Vector3f(0.f, 1.f, 0.f));
        glDrawArrays(GL_LINES, 0, (GLsizei)mPointsY.size());
        glDisableVertexAttribArray(positionLocation);
        
        // Draw Z axis.
        glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)mPointsZ.data());
        glEnableVertexAttribArray(positionLocation);
        mShaderProgram->setUniform(Consts::Shader::kColorUniformName, Vector3f(0.f, 0.f, 1.f));
        glDrawArrays(GL_LINES, 0, (GLsizei)mPointsX.size());
        glDisableVertexAttribArray(positionLocation);
        
        // Draw Y negative axis.
        glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)mPointsYNegative.data());
        glEnableVertexAttribArray(positionLocation);
        mShaderProgram->setUniform(Consts::Shader::kColorUniformName, Vector3f(0.f, 1.f, 0.f));
        glDrawArrays(GL_LINES, 0, (GLsizei)mPointsYNegative.size());
        glDisableVertexAttribArray(positionLocation);
    }
    
    void Axes3D::update(f32 dTime)
    {
        // Does nothing
    }
}
