//
//  FileUtils.mm
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "FileUtils.h"
#include <fstream>
#import <Foundation/Foundation.h>

namespace Plot3DLib
{
    namespace FileUtils
    {
        String loadNamedVertexShaderSource(const String &name)
        {
            NSString *nsName = [NSString stringWithUTF8String:name.c_str()];
            NSString *vertextShaderPathName = [[NSBundle mainBundle] pathForResource:nsName ofType:@"vsh"];
            return String([[NSString stringWithContentsOfFile:vertextShaderPathName encoding:NSUTF8StringEncoding error:nil] UTF8String]);
        }
        
        String loadNamedFragmentShaderSource(const String &name)
        {
            NSString *nsName = [NSString stringWithUTF8String:name.c_str()];
            NSString *vertextShaderPathName = [[NSBundle mainBundle] pathForResource:nsName ofType:@"fsh"];
            return String([[NSString stringWithContentsOfFile:vertextShaderPathName encoding:NSUTF8StringEncoding error:nil] UTF8String]);
        }
    }
}