//
//  Plot3D.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Plot3D.h"
#include "Consts.h"
#include "Camera3D.h"
#include "ShaderProgram.h"
#include "VBO.h"
#include "FileUtils.h"
#include <OpenGL/gl3.h>
#include <algorithm>

namespace Plot3DLib
{
    namespace
    {
        static const f32 kDisplayNextPointTimePeriod = 0.25f;
    }
    
    Plot3D::Plot3D()
    {
        initialize();
    }
    
    Plot3D::~Plot3D()
    {
    }
    
    bool Plot3D::isValid() const
    {
        const bool valid = (mShaderProgram && mShaderProgram->isValid());
        return valid;
    }
    
    void Plot3D::render(const Matrix4f &modelViewProjection)
    {
        if (!isValid())
            return;
        
        if (!mPointsBuffer->isValid())
            return;
        
        mShaderProgram->bind();
        
        mShaderProgram->setUniform(Consts::Shader::kModelViewProjectionMatrixUniformName, modelViewProjection);
        mShaderProgram->setUniform(Consts::Shader::kColorUniformName, mColor);
        
        glEnableVertexAttribArray(mShaderProgram->getAttribLocation("position"));
        mPointsBuffer->bind();
        mShaderProgram->setVertexAttrib("position", 3, 0, (f32*)0);
        mPointsBuffer->unbind();
        
//        const u32 points = u32(mPointsBuffer->getDataSize()) / sizeof(f32) / 3;
//        glDrawArrays(GL_LINE_STRIP, 0, points);
        glDrawArrays(GL_LINE_STRIP, 0, mRenderPointsCount);
        
        mShaderProgram->unbind();
    }
    
    void Plot3D::update(f32 dTime)
    {
        mAnimationTime += dTime;
        
        const s32 points = u32(mPointsBuffer->getDataSize()) / sizeof(f32) / 3;
        mRenderPointsCount = std::min(s32(mAnimationTime / kDisplayNextPointTimePeriod), points);
    }
    
    void Plot3D::setPointsBuffer(VBOPtr value)
    {
        mPointsBuffer = value;
        
        mRenderPointsCount = 0;
        mAnimationTime = 0.f;
    }
    
    void Plot3D::initialize()
    {
        String vertextShaderSource = FileUtils::loadNamedVertexShaderSource(Consts::kSimpleShaderName);
        String fragmentShaderSource = FileUtils::loadNamedFragmentShaderSource(Consts::kSimpleShaderName);
        mShaderProgram = ShaderProgram::create(Consts::kSimpleShaderName, vertextShaderSource, fragmentShaderSource, SIPairVec());
        
        mPointsBuffer.reset(new VBO(GL_ARRAY_BUFFER, 3));
        
        mColor = Vector3f(1.f, 1.f, 0.f);
    }
}
