//
//  FileUtils.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3d_File_Utils_h
#define Plot3d_File_Utils_h

#include "Types.h"

namespace Plot3DLib
{
    namespace FileUtils
    {
        //
        String loadNamedVertexShaderSource(const String &name);
        //
        String loadNamedFragmentShaderSource(const String &name);
    }
}

#endif
