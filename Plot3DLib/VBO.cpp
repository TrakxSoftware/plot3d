//
//  VBO.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "VBO.h"
#include <OpenGL/gl3.h>

namespace Plot3DLib
{
    VBO::VBO(u32 target, u16 dimension)
    {
        create(target, dimension);
    }
    
    VBO::~VBO()
    {
        destroy();
    }
    
    bool VBO::create(u32 target, u16 dimension)
    {
        glGenBuffers(1, &mId);
        mTarget = target;
        mDimension = dimension;
        return isValid();
    }
    
    bool VBO::destroy()
    {
        if (isValid())
        {
            glDeleteBuffers(1, &mId);
            mId = kInvalidBufferId;
            return true;
        }

        return false;
    }
    
    bool VBO::isValid() const
    {
        return (mId != kInvalidBufferId);
    }
    
    void VBO::bind() const
    {
        if (isValid())
            glBindBuffer(mTarget, mId);
    }
    
    void VBO::unbind() const
    {
        if (isValid())
            glBindBuffer(mTarget, 0);
    }
    
    bool VBO::attachData(const void *data, u32 dataSize, u32 usage)
    {
        if (!data || dataSize == 0)
            return false;
        
        mDataSize = dataSize;
        mUsage = usage;
        
        if (isValid())
        {
            bind();
            glBufferData(mTarget, dataSize, data, usage);
            unbind();
        }
        
        return false;
    }
    
//    void * VBO::lockDataToWrite()
//    {
//        return glMapBufferOES(mTarget, GL_WRITE_ONLY_OES);
//    }
//    
//    void VBO::unlockData()
//    {
//        glUnmapBufferOES(mTarget);
//    }
    
//    VBOPtr VBO::copy() const
//    {
//        // TODO :   so far we did not get any problems while copying buffer on background thread with shared GL context.
//        //          Need to pay attention on the behavior during operation the code, probably we need to add some locks.
//        
//        if (isValid())
//        {
//            bind();
//            
//            const void *pData = glMapBufferRangeEXT(mTarget, 0, mDataSize, GL_MAP_READ_BIT_EXT);
//            
//            VBOPtr vboCopy(new VBO(mTarget, mDimension));
//            vboCopy->attachData(pData, mDataSize, mUsage);
//
//            glUnmapBufferOES(mTarget);
//            
//            unbind();
//            
//            return vboCopy;
//        }
//        
//        return VBOPtr();
//    }
}