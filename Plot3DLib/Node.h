//
//  Node.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_Node_h
#define Plot3D_Node_h

#include "Types.h"

namespace Plot3DLib
{
    class Node
    {
    public:
        //
        Node();
        //
        virtual ~Node();
        //
        void addChildNode(NodePtr node);
        //
        void addSceneObject(SceneObjectPtr object);
        //
        void render(const Matrix4f &modelViewProjection);
        //
        void update(f32 dTime);
        
    protected:
    private:
        //
        Matrix4f mTransformation = Matrix4f().identity();
        //
        NodePtrVec mChildNodes;
        //
        SceneObjectPtrVec mSceneObjects;
    };
}

#endif
