//
//  ShaderProgram.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_ShaderProgram_h
#define Plot3D_ShaderProgram_h

#include "Types.h"


namespace Plot3DLib
{
    const u32 kInvalidShaderProgramId = 0;
    const u32 kInvalidShaderId = 0;
    
    class ShaderProgram
    {
    public:
        static ShaderProgramPtr create(const String & name, const String & vertexShaderCode, const String & fragmentShaderCode, const SIPairVec & preBindAttrinbs = SIPairVec());
        
        static void clearCache();
        
        virtual ~ShaderProgram();

        const String & getName() const;
        bool setUniform(const String & name, s32 value);
        bool setUniform(const String & name, f32 value);
        bool setUniform(const String & name, const Matrix4f & value);
        bool setUniform(const String & name, const Matrix3f & value);
        bool setUniform(const String & name, const Vector2f & value);
        bool setUniform(const String & name, const Vector3f & value);
        
        bool setVertexAttrib(const String & name, u16 dimension, const f32 * data);
        bool setVertexAttrib(const String & name, u16 dimension, s32 stride, const f32 * data);
        bool setVertexAttrib(s32 location, u16 dimension, s32 stride, const f32 *data);
        
        s32 getUniformLocation(const String & name);
        s32 getAttribLocation(const String & name);
        bool bindAttribLocation(const String & name, s32 location);
        
        void bind();
        void unbind();
        
        bool isValid() const;
        
    protected:
        ShaderProgram(const String & name, const String & vertexShaderCode, const String & fragmentShaderCode, const SIPairVec & preBindAttrinbs);
        
    private:
        bool create(const String & vertexShaderCode, const String & fragmentShaderCode, const SIPairVec & preBindAttrinbs);
        bool destroy();
        
        String mName;
        
        typedef std::map<String, s32> LocationMap;
        
        u32 mVertShader = kInvalidShaderId;
        u32 mFragShader = kInvalidShaderId;
        
        u32 mShaderProgram = kInvalidShaderProgramId;

        LocationMap mUniformLocations;
        LocationMap mAttribLocations;
    };
}

#endif
