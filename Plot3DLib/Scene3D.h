//
//  Scene3D.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#ifndef Plot3D_Scene3D_h
#define Plot3D_Scene3D_h

#include "Types.h"

namespace Plot3DLib
{
    class Scene3D
    {
    public:
        //
        Scene3D();
        //
        virtual ~Scene3D();
        //
        void renderAll();
        //
        void updateAll(f32 dTime);
        //
        void setRootNode(NodePtr value) { mRootNode = value; }
        //
        void rotate(f32 angle);
        //
        void scale(f32 scale);
        //
        Camera3DPtr getCamera() const { return mCamera; }
        
    protected:
    private:
        //
        Matrix4f calcProjectionMatrix() const;
        //
        Matrix4f calcModelViewMatrix() const;
        //
        Camera3DPtr mCamera;
        //
        NodePtr mRootNode;
        //
        f32 mAngle = 0.f;
        //
        f32 mScale = 1.f;
    };
}

#endif
