//
//  Texture2D.h
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//


#ifndef Plot3d_Texture_2d_h
#define Plot3d_Texture_2d_h

#include "Types.h"

namespace Plot3DLib
{
    class Texture2D
    {
    public:
        //
        Texture2D(size_t width, size_t height);
        //
        virtual ~Texture2D();
        //
        size_t getWidth() const { return mWidth; }
        //
        size_t getHeight() const { return mHeight; }
        //
        bool isValid() const;
        //
        void bind(TextureBindSlot slot = TBS_0);
        //
        void unbind();
        //
        size_t getId() const { return mId; }
        //
        bool generate();
        //
        bool destroy();
        
    protected:
        //
        void setId(size_t value) { mId = value; }
        
    private:
        //
        Texture2D(const Texture2D &) = delete;
        //
        Texture2D operator=(const Texture2D &) = delete;
        //
        size_t mId;
        //
        TextureBindSlot mBindSlot;
        //
        size_t mWidth;
        //
        size_t mHeight;
    };
}

#endif
