//
//  Scene3D.cpp
//  Plot3D
//
//  Created by SmartLivez on 8/24/15.
//  Copyright (c) 2015 SmartLivez. All rights reserved.
//

#include "Scene3D.h"
#include "Camera3D.h"
#include "Plot3D.h"
#include "Node.h"


namespace Plot3DLib
{
    Scene3D::Scene3D()
    : mCamera(new Camera3D)
    {
    }

    Scene3D::~Scene3D()
    {
    }
    
    void Scene3D::renderAll()
    {
        Matrix4f modelViewProjection = calcProjectionMatrix() * calcModelViewMatrix();
        if (mRootNode)
            mRootNode->render(modelViewProjection);
    }
    
    Matrix4f Scene3D::calcProjectionMatrix() const
    {
        Matrix4f mat = Matrix4f().identity();
        cml::matrix_perspective_xfov_RH(mat, 120.f, 1.f, 0.01f, 15000.f, cml::z_clip_zero);
        return mat;
    }
    
    Matrix4f Scene3D::calcModelViewMatrix() const
    {
        Matrix4f mat = mCamera->getCameraTransformationMatrix().inverse();
        
        Matrix4f rotMat = Matrix4f().identity();
        cml::matrix_rotation_axis_angle(rotMat, Vector3f(0.f, 1.f, 0.f), mAngle);
        mat = mat * rotMat;
        
        Matrix4f scaleMat = Matrix4f().identity();
        cml::matrix_scale(scaleMat, mScale, mScale, mScale);
        mat = mat * scaleMat;
        
        return mat;
    }
    
    void Scene3D::updateAll(f32 dTime)
    {
        if (mRootNode)
            mRootNode->update(dTime);
    }
    
    void Scene3D::rotate(f32 angle)
    {
        mAngle += angle;
    }
    
    void Scene3D::scale(f32 scale)
    {
        mScale *= scale;
        mScale = std::max(std::min(mScale, 10.f), 0.1f);
    }
}
